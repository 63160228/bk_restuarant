import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Salary } from './salary.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';

@Entity()
export class SalaryDetail {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  emp_name: string;
  @Column()
  emp_whours: number;
  @Column({ type: 'float' })
  emp_rate: number;
  @Column({ type: 'float' })
  emp_total: number;

  @ManyToOne(() => Salary, (salary) => salary.salaryDetailLists)
  salaryId: Salary;

  @ManyToOne(() => Employee, (employee) => employee.salDetail)
  employeeId: Employee;
  @OneToMany(() => Checkinout, (checkinout) => checkinout.salaryDetail)
  checkinout: Checkinout[];

  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
  @DeleteDateColumn()
  deletedAt: Date;
}
