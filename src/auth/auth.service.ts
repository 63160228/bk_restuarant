import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Employee } from 'src/employees/entities/employee.entity';
import { EmployeesService } from '../employees/employees.service';
import * as bcrypt from 'bcrypt';
@Injectable()
export class AuthService {
  constructor(
    private employeesService: EmployeesService,
    private jwtService: JwtService,
  ) {}

  async validateEmployees(username: string, pass: string): Promise<any> {
    // console.log(username);
    // console.log(pass);
    const employee = await this.employeesService.findOneByUsername(username);

    if (employee && employee.password === pass) {
      const { password, ...result } = employee;
      return result;
    }
    return null;
  }
  // async login(employee: any) {
  //   const payload = { username: employee.username, sub: employee.id };
  //   return {
  //     employee,
  //     access_token: this.jwtService.sign(payload),
  //   };
  // }
  async login(user: any) {
    const payload = { username: user.username, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
      user,
    };
  }
}
