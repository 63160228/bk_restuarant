import { IsNotEmpty } from 'class-validator';

export class CreateCheckinoutDto {
  @IsNotEmpty()
  employeeId: number;
  datetimeOut: Date;
  salaryDetailId: number;
}
