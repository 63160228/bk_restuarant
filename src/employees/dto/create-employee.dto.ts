import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  @Length(4, 32)
  username: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @IsNotEmpty()
  @Length(8, 32)
  password: string;

  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  birthday: Date;

  @IsNotEmpty()
  @Length(10, 60)
  address: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @Length(10, 30)
  @IsNotEmpty()
  email: string;

  oth_contact: string;

  @IsNotEmpty()
  start_date: Date;

  @IsNotEmpty()
  role: string;

  @IsNotEmpty()
  sal_rate: number;
}
