import { BillMaterialDetail } from './bill-detail';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Employee } from 'src/employees/entities/employee.entity';

@Entity()
export class BillMaterial {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  shop_name: string;
  @Column()
  date: Date;
  @Column({ type: 'float' })
  total: number;
  @Column({ type: 'float' })
  buy: number;
  @Column({ type: 'float' })
  change: number;
  @ManyToOne(() => Employee, (employee) => employee.billMaterials)
  employee: Employee;
  @OneToMany(() => BillMaterialDetail, (billItem) => billItem.bill)
  billItems: BillMaterialDetail[];
  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;
}
